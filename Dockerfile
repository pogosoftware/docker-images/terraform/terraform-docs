##########################################################################
### Builder
##########################################################################
FROM alpine:3.15.0 as builder
ARG VERSION=v0.16.0

WORKDIR /opt

RUN apk add --no-cache curl=7.80.0-r0 && \
    curl --fail --silent -L -o terraform-docs.tar.gz "https://github.com/terraform-docs/terraform-docs/releases/download/${VERSION}/terraform-docs-${VERSION}-linux-amd64.tar.gz" && \
    tar -zxf terraform-docs.tar.gz && \
    rm -rf /var/cache/apk/*

##########################################################################
### terraform-docs image
##########################################################################
FROM alpine:3.15.0
ARG VERSION=v0.16.0

COPY --from=builder ./opt/terraform-docs /usr/local/bin/terraform-docs
